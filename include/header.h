#ifndef HEADER_H
# define HEADER_H

#define _DEFAULT_SOURCE
#define _GNU_SOURCE

#include <limits.h>
#include <pthread.h>
#include <stddef.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

#define MAX_BUDDY 4096 //(size_t)sysconf(_SC_PAGESIZE)
#define MIN_BUDDY 64

struct meta
{
  int free;
  unsigned int size;
//  struct meta *next_free;
};


struct free_list
{
  size_t size;
  struct meta *first_free;
  struct free_list *next;
};

struct fb
{
  struct meta *next_free;
};

void *malloc(size_t size);
void free(void *ptr);
void *calloc(size_t number, size_t size);
void *realloc(void *ptr, size_t size);

void init_freel(void);
void init_size_list(size_t size, struct meta *add);
size_t aligned_to_size_t(size_t size);
struct meta *to_meta(void *a);
char *to_char(void *a);
struct meta *merge_bud(struct meta *ptr);
struct meta *get_bud(struct meta *ptr);
struct fb *to_fb(void *ptr);
void *buddy_rec(struct meta *big, size_t size);

struct free_list *g_freel;
extern struct free_list *g_freel;

pthread_mutex_t lock;
extern pthread_mutex_t lock;

#endif
