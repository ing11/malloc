YELL='\033[1;33m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'
bold=$(tput bold)
normal=$(tput sgr0)

tests='LD_PRELOAD=./libmalloc.so'
ref=""

print_c () {
  printf "%b%s%b" "$1" "$2" "${NC}"
}

cd tests

for dir in *; do
  [ ! -d "$dir" ] && continue
  print_c "$YELL" "========= $dir ========="
  echo
  for file in "$dir"/*.in; do
    echo
#    echo -n "" > tmp1
#    echo -n "" > tmp2
    arg1=$(head -n 1 "$file")
    arg2=$(tail -n 1 "$file")
    echo "$tests $arg1"
    echo "$arg2"
    outt=$(LD_PRELOAD=../libmalloc.so  $arg1 2>/dev/null)
    rett="$?"
    outr=$(LD_PRELOAD='' $arg2 2>/dev/null)
    retr="$?"
#    echo "$outt" > tmp1
#    echo "$outr" > tmp2

    if [ "$rett" -eq "$retr" ]; then
      printf '  %s - %s\n' "$(print_c "$GREEN" "OK")" "return value"
    else
      printf '  %s - %s\n' "$(print_c "$RED" "KO")" "return value"
    fi

    if [ "$outt" == "$outr" ]; then
      printf '  %s - %s\n' "$(print_c "$GREEN" "OK")" "output"
    else
      printf '  %s - %s\n' "$(print_c "$RED" "KO")" "output"
      echo
#      diff --color --unified tmp1 tmp2
#      exit 1
    fi
  done
  printf "\n"
done

print_c "$YELL" "======================"
echo
