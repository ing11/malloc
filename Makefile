CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -Werror \
       -fPIC -fvisibility=hidden -Iinclude
SRC=$(wildcard src/*.c)
OBJ=$(SRC:.c=.o)
LIB=libmalloc.so
EXEC=
TEST=tests/unit tests/recycle tests/recycle-2 tests/speed.py \
     tests/advancedrealloc tests/memoryfootprint tests/memoryfootprint-2

LD_FLAGS= -L. -lmalloc

all: $(OBJ) $(LIB)

$(LIB):
	$(CC) -shared $(OBJ) -o $(LIB)
$(EXEC): $(OBJ)


check: CFLAGS+=-g
check: all $(TEST)
	./tests/test.sh
	$(foreach var,$(TEST), LD_PRELOAD=./libmalloc.so ./$(var);)

clean: 
	$(RM) $(OBJ) $(LIB) tests/unit
