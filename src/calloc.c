#include "header.h"

__attribute__((__visibility__("default")))
void *calloc(size_t nmemb, size_t size)
{
  pthread_mutex_lock(&lock);
  void *ret = malloc(nmemb * size);
  if (ret)
    ret = memset(ret, 0, nmemb * size);
  pthread_mutex_unlock(&lock);
  return ret;
}
