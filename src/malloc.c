#include "header.h"

pthread_mutex_t lock = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;

char *to_char(void *a)
{
  char *aa = a;
  return aa;
}

size_t aligned_to_size_t(size_t size)
{
  if (!(size % sizeof (size_t)))
    return size;
  size = size + (sizeof (size_t) - (size % sizeof (size_t)));
  return size;
}

static void *return_sup(size_t size)
{
  struct meta *ret;
  ret = mmap(NULL, aligned_to_size_t(size) + sizeof (struct meta), \
             PROT_READ | PROT_WRITE,                               \
             MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (ret == MAP_FAILED)
    return NULL;
  ret->size = aligned_to_size_t(size);
  struct fb *tmp = to_fb(ret + 1);
  tmp->next_free = NULL;
  return ret + 1;
}

void *buddy_rec(struct meta *big, size_t size)
{
  big->size = ((big->size + sizeof (struct meta)) / 2) - sizeof (struct meta);
  big->free = 1;
  init_size_list(big->size, big);
  struct meta *bud = get_bud(big);
  bud->size = big->size;
  bud->free = 0;
  size_t tmp = (bud->size + sizeof (struct meta))/2;
  if (size + sizeof (struct meta) <= tmp && MIN_BUDDY < tmp)
    return buddy_rec(bud, size);
  return bud + 1;
}

static void *create_buddy(size_t size)
{
  struct meta *buddy = mmap(NULL, MAX_BUDDY, PROT_READ | PROT_WRITE, \
                            MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (buddy == MAP_FAILED)
    return NULL;
  buddy->size = MAX_BUDDY - sizeof (struct meta);
  if (size <= MAX_BUDDY/2 - sizeof (struct meta))
    return buddy_rec(buddy, size);
  return buddy + 1;
}

static void *malloc_part(struct free_list *cur, size_t size)
{
  struct meta *ret;
  ret = cur->first_free;
  ret->free = 0;
  struct fb *tempo = to_fb(ret + 1);
  cur->first_free = tempo->next_free;
  size_t tmp = ret->size + sizeof (struct meta);
  if (tmp > MIN_BUDDY && tmp <= MAX_BUDDY && \
      (tmp / 2 >= size + sizeof (struct meta)))
    return buddy_rec(ret, size);
  else
    return ret + 1;
}

__attribute__((__visibility__("default")))
void *malloc(size_t size)
{
  if (size == 0)
    return NULL;
  pthread_mutex_lock(&lock);
  init_freel();
  void *result;
  struct free_list *cur = g_freel;
  for (; cur; cur = cur->next)
  {
    if (cur->size >= size && cur->first_free)
    {
      result = malloc_part(cur, size);
      pthread_mutex_unlock(&lock);
      return result;
    }
  }
  if (size > MAX_BUDDY)
    result = return_sup(size);
  else
    result = create_buddy(size);
  pthread_mutex_unlock(&lock);
  return result;
}
