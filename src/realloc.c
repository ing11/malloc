#include "header.h"

__attribute__((__visibility__("default")))
void *realloc(void *ptr, size_t size)
{
  if (!ptr)
    return malloc(size);
  char *pt = ptr;
  pt = pt - sizeof (struct meta);
  struct meta *cur = to_meta(pt);
  if (cur->size >= size)
    return ptr;
  else
  {
    pthread_mutex_lock(&lock);
    void *ret = malloc(size);
    if (size < cur->size)
      ret = memcpy(ret, ptr, size);
    else
      ret = memcpy(ret, ptr, cur->size);
    free(ptr);
    pthread_mutex_unlock(&lock);
    return ret;
  }
}
