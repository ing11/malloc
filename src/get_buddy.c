#include "header.h"

struct fb *to_fb(void *ptr)
{
  struct fb *ret = ptr;
  return ret;
}

struct meta *get_bud(struct meta *ptr)
{
  char *begin = NULL;
  char *p = to_char(ptr);
  size_t addr = p - begin;
  size_t x = ptr->size + sizeof (struct meta);
  addr = addr ^ x;
  char * ret = begin + addr;
  struct meta *bud = to_meta(ret);
  return bud;
}

void del_ptr(struct meta *ptr)
{
  struct free_list *fl = g_freel;
  for (; fl->next && fl->next->size <= ptr->size; fl = fl->next)
    continue;
  struct meta *cur = fl->first_free;
  struct fb *tmp = to_fb(cur + 1);
  if (cur - ptr == 0)
  {
    fl->first_free = tmp->next_free;
    return;
  }
  for (tmp = to_fb(cur + 1); cur; tmp = to_fb(cur + 1), cur = tmp->next_free)
  {
    if (ptr - tmp->next_free == 0)
    {
      struct fb *tmp2 = to_fb(tmp->next_free + 1);
      tmp->next_free = tmp2->next_free;
      return;
    }
  }
}

struct meta *merge_bud(struct meta *ptr)
{
  struct meta *bud = get_bud(ptr);
  if (ptr->size >= MAX_BUDDY/2 || bud->size != ptr->size || !bud->free)
    return ptr;
  size_t tmp = 2 * (bud->size + sizeof (struct meta)) - sizeof (struct meta);
  del_ptr(bud);
  if (bud - ptr < 0)
  {
    bud->size = tmp;
    return bud;
  }
  else
  {
    ptr->size = tmp;
    return ptr;
  }
}


