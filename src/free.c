#include "header.h"

struct meta *to_meta(void *a)
{
  struct meta *aa = a;
  return aa;
}

void init_size_list(size_t size, struct meta *add)
{
  struct free_list *fl = g_freel;
  for (; fl->next && fl->next->size <= size; fl = fl->next)
    continue;
  int tmp = MAX_BUDDY;
  struct fb *tempo = to_fb(add + 1);
  if (!fl->next && fl->size < size && to_char(fl + 2) - to_char(g_freel) < tmp)
  {
        if (add)
      tempo->next_free = NULL;
    fl->next = fl + 1;
    fl->next->size = size;
    fl->next->first_free = add;
    fl->next->next = NULL;
  }
  else
  {
    if (add)
      tempo->next_free = fl->first_free;
    fl->first_free = add;
  }
}

void init_freel(void)
{
  if (g_freel)
    return;
  struct free_list *tmp;
  tmp = mmap(NULL, MAX_BUDDY, PROT_READ | PROT_WRITE, \
             MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (tmp == MAP_FAILED)
    return;
  tmp->size = 0;
  tmp->next = NULL;
  tmp->first_free = NULL;
  g_freel = tmp;
  for (size_t i = MIN_BUDDY; i <= MAX_BUDDY; i *= 2)
    init_size_list(i - sizeof(struct meta), NULL);
}

__attribute__((__visibility__("default")))
void free(void *ptr)
{
  if (!ptr)
    return;
  pthread_mutex_lock(&lock);
  init_freel();
  char *f = ptr;
  f = f - sizeof (struct meta);
  struct meta *cur = to_meta(f);
  if (cur->size < MAX_BUDDY/2)
  {
    struct meta *tmp = merge_bud(cur);
    for (; tmp - cur; cur = tmp)
      tmp = merge_bud(tmp);
  }
  cur->free = 1;
  init_size_list(cur->size, cur);
  pthread_mutex_unlock(&lock);
}
